<?php

namespace App\Http\DataObjects;


class TestDO
{

  public $name;
  public $email;

    /**
     * Set name data mamber 
     *
     * @return NUll
     */
    public  function set_name($name)
    {
       $this->name = $name;
   
    }
    
    /**
     * Set email data mamber 
     *
     * @return NUll
     */
    public  function set_email($email)
    {
       $this->email = $email;
   
    }

    /**
     * Get name data mamber 
     *
     * @return object
     */

    public  function get_name()
    {
       return $this->name;
   
    }
  

    /**
     * Get email data mamber 
     *
     * @return object
     */

    public  function get_email()
    {
      return  $this->email;
   
    }
}
