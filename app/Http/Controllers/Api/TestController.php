<?php

namespace App\Http\Controllers\Api;

//use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\TestStoreRequest;
use TestService;  // Register Through Service Provider

use App\Http\DataObjects\TestDO;

class TestController extends Controller
{
    

    Public function getList()
    {
    	 $arr = [];
    	 return $arr =  ['name' => 'Welcome Listing Page'];
        
    }

    public function saveData(TestStoreRequest $request, TestService $testService)
    {
       
       // return $request->all();

         $validated = $request->validated();
        
         if (isset($request->validator) && $request->validator->fails()) {
            
            return response()->json($request->validator->messages(), 400);
            
            }

            $testDO = new TestDO();
            $testDO->set_name($validated['name']);
            $testDO->set_email($validated['email']);

        // Service Code Started 
         $testService = New TestService();
         $service_response = $testService->add_new_test($testDO);
        
        
        return $service_response;
        
     }


}
