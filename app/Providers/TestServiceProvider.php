<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Http\Services\TestService;

class TestServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(TestService::class, function($app){
              return new TestService();
        });
    }
}
