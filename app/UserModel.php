<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserModel extends Model
{
    
    
    protected $table = "user";
 	
    protected $primaryKey = 'id';
  
    public $timestamps = false;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'created_at', 'updated_at'
    ];


    Public function save_data($data)
    {  
        $save_data = [
            'name' => $data->get_name(),
            'email' => $data->get_email(),
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ];
      
       return $response = $this->create($save_data);
       


    }


}
