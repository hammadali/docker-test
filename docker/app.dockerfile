FROM php:7.1-fpm

RUN printf "deb http://archive.debian.org/debian/ jessie main\ndeb-src http://archive.debian.org/debian/ jessie main\ndeb http://security.debian.org jessie/updates main\ndeb-src http://security.debian.org jessie/updates main" > /etc/apt/sources.list


RUN apt-get update && apt-get upgrade && apt-get install -y libmcrypt-dev \
    && docker-php-ext-install mcrypt pdo_mysql
